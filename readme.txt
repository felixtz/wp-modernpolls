=== Modern Polls ===
Contributors:felixtz
Donate link: 
Tags: polls, surveys, poll, modern polls, moderne umfragen, umfragen
Requires at least: 4.0
Tested up to: 4.9.8
Requires PHP: 5.2.4
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
Modern Polls is easy to use, customizable by Themes or CSS and has an modern backend.
You take the control of how users are tracked! It supports multiple and single answers. 
 
== Description ==
 
Modern Polls is easy to use! 
You can customise the default Poll-Theme to your needs and Install multiple different versions of your own Theme.
The Backend is a modern, userfriendly and self-explaning Interface.
A Poll can have a Startdate and Time as well as an Enddate and time, automatically!
Use the Polls on Posts or whole Pages.

Review the Voters and Analysis of your Polls in the Backend Info. It includes the number of Voters as well as analysis in Charts.

Modern Polls uses its own Database Tables for better Backups, Exports, selfmade analysis and data integrity.
 
== Screenshots ==
 
1. Polls Overview
2. Create Poll
3. Poll Examples
 
== Installation ==

Just install via Plugin Manager and start to create your first Poll. Thats it!
 

 