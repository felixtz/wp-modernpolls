<?php
/********************************************************************
 * @plugin     ModernPolls
 * @date       15.04.2018
 * @author     Felix Tzschucke <f.tzschucke@gmail.com>
 * @copyright  2018 Felix Tzschucke
 * @license    GPL2
 * @version    1.0.0 Release
 * @link       https://felixtz.de/
 ********************************************************************/

/*
Plugin Name: Modern Polls
Plugin URI:
Description: Modern AJAX Polls with different or your own Theme
Version: 1.0.0
Author: Felix Tz
Author URI: https://felixtz.de/
Text Domain: /resources/lang
*/

define( 'FelixTzWPModernPollsVersion', '1.0.0' );
define( 'FelixTzWPModernPollsDir', plugin_dir_path( __FILE__ ) );
define( 'FelixTzWPModernPollsView', FelixTzWPModernPollsDir.'/resources/views/' );
define( 'FelixTzWPModernPollsFile', __FILE__ );
define( 'FelixTzWPModernPollsDirName', dirname( plugin_basename( __FILE__ ) ) );
define( 'FelixTzWPModernPollsTextdomain', 'modern-polls' );

require "vendor/autoload.php";

use \FelixTzWPModernPolls\App;

new App();