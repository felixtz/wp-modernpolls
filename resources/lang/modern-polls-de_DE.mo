��    ^           �      �  
   �  	             *     1     >     B     R     X     _     l     s          �     �     �     �     �  	   �     �     �     �  V   �     F	  	   e	     o	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     7
  
   M
     X
     `
  "   z
     �
     �
     �
     �
     �
  	   �
     �
     �
          	          0     K     P     ]     j     r     �     �     �     �     �     �     �     �     �     �     �     �       
   "     -     4     =  	   M     W  G   l      �     �     �  g   �     Y     f     k     r     �      �  C   �     �  M  �     E     Y     g     �     �     �     �  
   �     �     �  	   �     �     �                    7  
   K     V     i     q     x  g   �  '   �       (   +  
   T     _     c  
   i     t     �     �     �     �  %   �  4   �     ,  !   K  $   m     �  	   �     �     �     �     �               #     2  #   C     g     m  
   v  #   �  !   �     �     �     �     �     �               +     <  	   B     L  
   U  	   `     j     �  %   �  $   �  	   �  	   �     �     �  #   �           )  ]   F  )   �     �     �  �   �     �  	   �           	     *  +   -  8   Y     �         B          2                '       S          0          P   I   =   @                  .   ;      O   ,   G   ?   5      (   W   T      "          4   %   >   	   :   6   C       *            #   <   ]      
                        N   -       &   D           H       7   /   8          J           R             V          1   )   ^   E   F                     Q                      +       $   !   Z   U   M   A   9   \      [   Y       3   X           K         L    Add Answer All Polls Allow more than one Answer? Answer Back to Vote Bad Can Be Improved Close Closed Closed Polls Create Create Poll Date Default Delete Display disabled Form Display the Result Edit Edit Poll End Datetime End Plan Enter Poll ID Error, please Contact the Administrator and tell him the the Error-Code %s. Thank you. Error: Poll ID must be numeric Excellent Failed To Verify Referrer Future Good Help Hide How do you like this plugin? Info Insert Poll Install Invalid Poll ID Invalid Poll ID. Poll ID #%s Log Time Log Users by Cookie Log Users by IP-Address Log Users by Username Log Voters Logging Maximum Number of Answers Maximum number of choices allowed: Modern Polls Multiple Answers Never show Result No No Comments No Expiry No Polls. Create one. Open Options Overview Please choose a valid answer. Please enter Poll ID again Poll Poll Answers Poll Content Poll ID Poll Question Poll Shortcut Poll created Publish Question Remove Result Handle Results Save Save changes Settings Show Result after Vote Show Result before Vote Start Datetime Start Plan Status Template Template select Templates This Poll never Ends This field cannot be empty. But if you dont need it, you can delete it! This is a sample Text in an Poll Time Time until Log expires To use this Poll in Frontend, use the Poll Button in the Text Editor or copy & paste the Poll shortcut. View Results Vote Voters When Poll is Closed Yes You must input an Poll Question. Your last request is still being processed. Please wait a while ... default Project-Id-Version: ModernPolls 1.0.0
POT-Creation-Date: 2018-04-15 20:39:11+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-08-17 23:01+0200
Language-Team: TEAM
X-Generator: Poedit 2.0.7
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de
 Antwort hinzufügen Alle Umfragen Mehrere Antworten erlauben? Antwort Zurück Schlecht Könnte verbessert werden Schließen Geschlossen Geschlossene Umfragen Erstellen Umfrage erstellen Datum Standard Löschen Deaktiviertes Formular anzeigen Ergebnisse anzeigen Bearbeiten Umfrage bearbeiten Endzeit Ablauf Umfragen ID eingeben Fehler, bitte kontaktieren Sie den Administrator und teilen Sie ihm den Fehlercode %s mit. Vielen Dank. Fehler: Umfragen ID muss numerisch sein Ausgezeichnet Referrer konnte nicht überprüft werden Zukünftig Gut Hilfe Verstecken Wie gefällt dir dieses Plugin? Info Umfrage hinzufügen Installieren Ungültige Umfragen ID Ungültige Umfrage-ID. Umfrage-ID #%s Protokollzeit (wie lange Wähler gespeichert werden) Wähler über Cookies erfassen Wähler über IP-Adresse erfassen Wähler über Benutzernamen erfassen Wähler protokollieren Protokoll Anzahl erlaubter Antworten Maximale Anzahl an Antworten: Umfragen Mehrfach Antworten Ergebnis nie anzeigen Nein Kein Kommentar kein Ablaufdatum Keine Umfragen. Erstellen Sie eine. Offen Optionen Übersicht Bitte wählen Sie eine Antwort aus. Bitte Umfragen ID erneut eingeben Umfrage Umfrage Antworten Umfrage Inhalt Umfragen ID Abstimmungsfrage Verknüpfung Umfrage erstellt Veröffentlichen Frage Entfernen Ergebnis Ergebnisse Speichern Änderungen speichern Einstellungen Ergebnis nach der Abstimmung anzeigen Ergebnis vor der Abstimmung anzeigen Startzeit Startplan Status Vorlage Vorlage zum Installieren auswählen Vorlagen Diese Umfrage soll nie enden Dieses Feld darf nicht leer sein, aber wenn Sie es nicht benötigen können Sie es entfernen. Dies ist ein Beispiel Text einer Umfrage. Zeit Zeit bis das Protokoll abläuft Um diese Umfrage im Frontend anzuzeigen, verwenden Sie im Texteditor den Button zum hinzufügen einer Umfrage und tragen Sie die Umfragen ID ein oder kopieren Sie direkt die Verknüpfung und fügen Sie diese auf der gewünschten Seite ein. Ergebnisse anzeigen Abstimmen Wähler Wenn die Umfrage geschlossen ist Ja Sie müssen eine Abstimmungsfrage eingeben. Ihre letzte Anfrage wird noch bearbeitet. Bitte warten.. default 