/********************************************************************
 * @plugin     ModernPolls
 * @file       resources/asstes/js/modern-polls.js
 * @date       15.04.2018
 * @author     Felix Tzschucke <f.tzschucke@gmail.com>
 * @copyright  2018 Felix Tzschucke
 * @license    GPL2
 * @version    1.0.0 Release
 * @link       https://felixtz.de/
 ********************************************************************/

var pending  = false;
var multiple = false;
var answers  = [];


function mpp_vote(hash)
{
    if(!pending)
    {
        pending = true;
        answers = [];

        var hash  = hash;

        var mpp      = '#mpp_' + hash;

        var id    = jQuery(mpp).find('#mpp_id').val();
        var nonce = jQuery(mpp).find('#mpp_nonce').val();

        var singlePoll = mpp + ' .mpp-default_radio_input';
        var multiplePoll = mpp + ' .mpp-default_checkbox_input';


        if ( jQuery( multiplePoll ).length > 0)
        {
            multiple = true;
            jQuery( multiplePoll ).each(function(i) {
                if (jQuery(this).is(':checked'))
                {
                    answers.push( parseInt(jQuery(this).val()) );
                }
            });
        }else{
            jQuery(singlePoll).each(function(i)
            {
                if (jQuery(this).is(':checked'))
                {
                    answers.push( parseInt(jQuery(this).val()) );
                }
            });
        }

        jQuery('html,body').animate({ scrollTop: jQuery(mpp).offset().top}, 'slow');

        if(answers.length > 0)
        {
            answers = JSON.stringify(answers);
            jQuery.ajax({
                type: 'POST',
                xhrFields: {withCredentials: true},
                url: modernpollsL10n.ajax_url,
                data: 'action=mppVote&mpp_id=' + id + '&mpp_answers=' + answers + '&mpp_hash=' + hash + '&mpp_nonce=' + nonce,
                cache: false,
                success: function(data){
                    pending = false;
                    jQuery(mpp).replaceWith(data);
                }
            });
        }
        else
        {
            pending = false;
            alert(modernpollsL10n.text_valid);
        }

    } else {
        alert(modernpollsL10n.text_wait);
    }
}

function mpp_result(hash, backLink = false) {

    var hash  = hash;

    var mpp      = '#mpp_' + hash;

    var id    = jQuery(mpp).find('#mpp_id').val();
    var nonce = jQuery(mpp).find('#mpp_nonce').val();

    if ( !backLink ) {
        jQuery.ajax({
            type: 'POST',
            xhrFields: {withCredentials: true},
            url: modernpollsL10n.ajax_url,
            data: 'action=mppResult&mpp_id=' + id + '&mpp_hash=' + hash + '&mpp_nonce=' + nonce,
            cache: false,
            success: function (data) {
                jQuery(mpp).hide();

                if (jQuery(mpp + '_result').length < 1) {
                    jQuery(mpp).after(data);
                } else {
                    jQuery(mpp + '_result').replaceWith(data);
                    jQuery(mpp + '_result').show();
                }
            }
        });
    } else {
        jQuery(mpp + '_result').remove();
        jQuery(mpp).show();
    }
}