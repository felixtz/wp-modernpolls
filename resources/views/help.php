<?php
/********************************************************************
 * @plugin     ModernPolls
 * @file       views/help.php
 * @date       15.04.2018
 * @author     Felix Tzschucke <f.tzschucke@gmail.com>
 * @copyright  2018 Felix Tzschucke
 * @license    GPL2
 * @version    1.0.0 Release
 * @link       https://felixtz.de/
 ********************************************************************/
