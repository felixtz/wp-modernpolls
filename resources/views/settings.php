<?php
/********************************************************************
 * @plugin     ModernPolls
 * @file       views/settings.php
 * @date       15.04.2018
 * @author     Felix Tzschucke <f.tzschucke@gmail.com>
 * @copyright  2018 Felix Tzschucke
 * @license    GPL2
 * @version    1.0.0 Release
 * @link       https://felixtz.de/
 ********************************************************************/

$log_ip     = $settings->log_ip;
$log_cookie = $settings->log_cookie;
$log_user   = $settings->log_user;
$log_time   = $settings->log_expire;

$closed_poll = $settings->closed_poll;

?>
<div class="mpp-body_wrapper">
    <div class="mpp-container">
        <form method="post" action="<?php echo esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>">
            <div class="mpp-container_head mpp-border_bottom">
                <h2 class=""><?php _e('Settings', FelixTzWPModernPollsTextdomain); ?></h2>

                <div class="">
                    <button type="submit" name="do" value="save" class="mpp-btn mpp-btn_primary"><?php _e('Save changes', FelixTzWPModernPollsTextdomain) ?></button>
                </div>
                <div class="mpp-clearfix"></div>
            </div>
            <ul class="mpp-nav mpp-nav_tabs">
                <li class="mpp-nav_item">
                    <a class="mpp-nav_link mpp-active" data-href="poll"><?php _e('Poll', FelixTzWPModernPollsTextdomain) ?></a>
                </li>
                <li class="mpp-nav_item">
                    <a class="mpp-nav_link" data-href="logging"><?php _e('Logging', FelixTzWPModernPollsTextdomain) ?></a>
                </li>
            </ul>
            <div class="mpp-tab_content">
                <div class="mpp-tab_pane mpp-tab_pane_fade mpp-tab_pane_show mpp-active" id="mpp-poll">
                    <span class="small spacer_bottom"><?php _e('Closed Polls', FelixTzWPModernPollsTextdomain) ?></span>
                    <div class="mpp-input_group spacer_bottom">
                        <div class="mpp-input_group_prepend" style="flex: 1;display: block;">
                            <div class="mpp-input_group_text"><?php _e('When Poll is Closed', FelixTzWPModernPollsTextdomain) ?></div>
                        </div>
                        <select name="mpp_poll_closed" class="mpp-select">
                            <option value="0" <?=($closed_poll==0)?'selected':'';?>><?php _e('Display the Result', FelixTzWPModernPollsTextdomain); ?></option>
                            <option value="1" <?=($closed_poll==1)?'selected':'';?>><?php _e('Display disabled Form', FelixTzWPModernPollsTextdomain); ?></option>
                            <option value="2" <?=($closed_poll==2)?'selected':'';?>><?php _e('Hide', FelixTzWPModernPollsTextdomain); ?></option>
                        </select>
                    </div>

                </div>
                <div class="mpp-tab_pane mpp-tab_pane_fade" id="mpp-logging">
                    <span class="small spacer_bottom"><?php _e('Log Voters', FelixTzWPModernPollsTextdomain) ?></span>
                    <div class="mpp-input_group spacer_bottom">
                        <div class="mpp-input_group spacer_bottom">
                            <div class="mpp-input_group_prepend" style="flex:1;display: block;">
                                <div class="mpp-input_group_text"><?php _e('Log Users by IP-Address', FelixTzWPModernPollsTextdomain) ?></div>
                            </div>
                            <input type="checkbox" id="logbyip" name="mpp_log_ip" value="1" class="mpp-input mpp-checkbox_xl" <?=($log_ip)?'checked':''; ?>>
                        </div>
                        <div class="mpp-input_group spacer_bottom">
                            <div class="mpp-input_group_prepend" style="flex:1;display: block;">
                                    <div class="mpp-input_group_text"><?php _e('Log Users by Cookie', FelixTzWPModernPollsTextdomain) ?></div>
                            </div>
                            <input type="checkbox" id="logbycookie" name="mpp_log_cookie" value="1" class="mpp-input mpp-checkbox_xl" <?=($log_cookie)?'checked':''; ?>>
                        </div>
                        <div class="mpp-input_group spacer_bottom">
                            <div class="mpp-input_group_prepend" style="flex:1;display: block;">
                                <div class="mpp-input_group_text"><?php _e('Log Users by Username', FelixTzWPModernPollsTextdomain) ?></div>
                            </div>
                            <input type="checkbox" id="logbyuser" name="mpp_log_user" value="1" class="mpp-input mpp-checkbox_xl" <?= ($log_user)?'checked':''; ?>>
                        </div>
                    </div>

                    <span class="small spacer_bottom"><?php _e('Log Time', FelixTzWPModernPollsTextdomain) ?></span>
                    <div class="mpp-input_group spacer_bottom">
                        <div class="mpp-input_group spacer_bottom">
                            <div class="mpp-input_group_prepend" style="flex:1;display: block;">
                                <div class="mpp-input_group_text" style="height: 24px;"><?php _e('Time until Log expires', FelixTzWPModernPollsTextdomain) ?></div>
                            </div>
                            <input style="text-align: center" type="text" pattern="[0-9]{1,}" id="logtime" name="mpp_log_time" class="mpp-input" value="<?=$log_time?>">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>