<?php
/********************************************************************
 * @plugin     ModernPolls
 * @file       app/Controllers/SettingsController.php
 * @date       15.04.2018
 * @author     Felix Tzschucke <f.tzschucke@gmail.com>
 * @copyright  2018 Felix Tzschucke
 * @license    GPL2
 * @version    1.0.0 Release
 * @link       https://felixtz.de/
 ********************************************************************/

namespace FelixTzWPModernPolls\Controllers;

use FelixTzWPModernPolls\Models\Settings;


class SettingsController {

    public $settings;

    public function __construct() {
        $this->settings = new Settings();
    }

    public function get($column) {
        return $this->settings->get($column);
    }

    public function getAll() {
        return $this->settings->getAll();
    }

    public function save($post) {

        $log_ip     = $post['mpp_log_ip'];
        $log_cookie = $post['mpp_log_cookie'];
        $log_user   = $post['mpp_log_user'];
        $log_expire = $post['mpp_log_time'];
        $closed_poll= $post['mpp_poll_closed'];

        return $this->settings->save($log_ip, $log_cookie, $log_user, $log_expire, $closed_poll);
    }
}