<?php
/********************************************************************
 * @plugin     ModernPolls
 * @file       app/Models/Templates.php
 * @date       15.04.2018
 * @author     Felix Tzschucke <f.tzschucke@gmail.com>
 * @copyright  2018 Felix Tzschucke
 * @license    GPL2
 * @version    1.0.0 Release
 * @link       https://felixtz.de/
 ********************************************************************/

namespace FelixTzWPModernPolls\Models;


class Templates extends Model {

    public function create() {

        $qry = "CREATE TABLE ".$this->db->mp_templates." (".
            "id int(10) NOT NULL auto_increment," .
            "name varchar(200) character set utf8 NOT NULL default ''," .
            "dir varchar(200) NOT NULL default ''," .
            "PRIMARY KEY  (id)" .
            ") $this->charsetCollate;";
        dbDelta( $qry );
    }

    public function sampleData() {

        $exc = $this->db->insert(   $this->db->mp_templates,
            array(   'name' => __( 'Default', FelixTzWPModernPollsTextdomain ),
                     'dir'  => __( 'default', FelixTzWPModernPollsTextdomain ) ),
            array( '%s', '%s' ) );

        return $exc;
    }

    public function insert($name, $dir) {
        $exc = $this->db->insert(   $this->db->mp_templates,
            array(   'name' => $name,
                     'dir'  => $dir ),
            array( '%s', '%s' ) );

        return $exc;
    }

    public function getAll() {
        $qry = $this->db->get_results( "SELECT * FROM ".$this->db->mp_templates." " );
        return $qry;
    }

    public function get($id) {
        $qry = $this->db->get_row( "SELECT * FROM ".$this->db->mp_templates." WHERE id = ".$id." ");
        return $qry;
    }

    public function delete($id) {
        $qry = $this->db->delete( $this->db->mp_templates, array( 'id' => $id ) );
        if ($qry) {
            return true;
        } else {
            return false;
        }
    }
}